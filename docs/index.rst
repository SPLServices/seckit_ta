.. Seckit Technology Addons for Splunk documentation master file, created by
   sphinx-quickstart on Sun Oct  7 13:30:23 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Seckit Technology Addons for Splunk's documentation!
===============================================================

Success Enablement Content Technology Add ons is a sub project of SecKit, content designed to get you up and running on Splunk finding insights faster. The project wraps Splunk Add ons with prescribed configuration suitable for most organizations. Allowing optimal use of your infrastructure and license capacity.

Microsoft Windows
--------------------------

The following kit contains guidance for Splunk TA Windows 6.0.0.

- :doc:`Splunk TA Windows <tawindows:index>`
- Splunk_TA_microsoft_dns is obsolete and has been merged into Splunk_TA_windows
- Splunk_TA_microsoft_ad is obsolete and has been merged into Splunk_TA_windows

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
